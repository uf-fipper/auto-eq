# 自动增加判断枚举相等和比较的方法

`AutoEq` 宏自动生成 `==` 和 `!=` 方法

`AutoOrd` 宏自动生成 `<` `>` `<=` `>=` 方法

```cj
from auto_eq import auto_eq.{AutoEq, AutoOrd}

@AutoEq
@AutoOrd
enum TestEnum {
    | A(Int, Int)
    | B
}

// 支持泛型
@AutoEq
@AutoOrd
enum TestEnum2<T> where T <: Ord<T> {
    A(T)
}

main(): Int {
    println(TestEnum.A(1, 1) == TestEnum.A(1, 1))
    println(TestEnum.A(1, 1) != TestEnum.A(1, 2))
    println(TestEnum.A(1, 1) < TestEnum.A(1, 2))
    println(TestEnum.A(1, 1) < TestEnum.B)

    println(TestEnum2<Int>.A(1) < TestEnum2<Int>.A(2))
    return 0
}
```
